#pragma semicolon 1

#include <sourcemod>
#include <s-a-s>

#pragma newdecls required

#define VERSION "1.2.0"

char g_sAdminDatabase[PLATFORM_MAX_PATH];
char g_sNicknameDatabase[PLATFORM_MAX_PATH];

public Plugin myinfo = 
{
	name = "S.A.S. - Simple Administration System", 
	author = "RockZehh", 
	description = "A simplistic administration system for SourceMod. - Modded for Raging Scout DM", 
	version = VERSION, 
	url = "https://bitbucket.org/rockzehh/ragingscout-dm"
};

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int iErr_max)
{
	CreateNative("SAS_CheckAdmin", Native_CheckAdmin);
	
	CreateNative("SAS_GetAdmin", Native_GetAdmin);
	CreateNative("SAS_GetNickname", Native_GetNickname);
	
	CreateNative("SAS_SetAdmin", Native_SetAdmin);
	CreateNative("SAS_SetNickname", Native_SetNickname);
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	char sPath[PLATFORM_MAX_PATH];
	
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/simple-administration-system/");
	if (!DirExists(sPath))
		CreateDirectory(sPath, 511);
	
	BuildPath(Path_SM, g_sAdminDatabase, sizeof(g_sAdminDatabase), "configs/simple-administration-system/admins.txt");
	if (!FileExists(g_sAdminDatabase))
		PrintToServer("[S.A.S.] Admin database was not found! Creating admin database.");
	
	BuildPath(Path_SM, g_sNicknameDatabase, sizeof(g_sNicknameDatabase), "configs/simple-administration-system/nicknames.txt");
	if (!FileExists(g_sNicknameDatabase))
		PrintToServer("[S.A.S.] Nickname database was not found! Creating nickname database.");
	
	RegConsoleCmd("sm_banplayer", Command_BanPlayer, "Bans a player.");
	RegConsoleCmd("sm_createadmin", Command_CreateAdmin, "Creates an admin.");
	RegConsoleCmd("sm_setnickname", Command_SetNickname, "Sets the nickname of a player.");
	RegConsoleCmd("sm_unbanplayer", Command_UnBanPlayer, "Unbans a player.");
	
	LoadTranslations("common.phrases");
}

//Plugin Commands:
public Action Command_BanPlayer(int iClient, int iArgs)
{
	char sArg[65], sArguments[256], sTime[12];
	int iArg, iNext_Arg;
	
	if (iArgs < 3)
	{
		ReplyToCommand(iClient, "[S.A.S.] sm_banplayer <target> <time> <reason>");
		return Plugin_Handled;
	}
	
	GetCmdArgString(sArguments, sizeof(sArguments));
	
	iArg = BreakString(sArguments, sArg, sizeof(sArg));
	
	if (iClient)
	{
		if (SAS_CheckAdmin(iClient))
		{
			if (SAS_GetAdmin(iClient) >= 2)
			{
				int iTarget = FindTarget(iClient, sArg, false, false);
				
				if (iTarget == -1)
				{
					return Plugin_Handled;
				}
				
				if ((iNext_Arg = BreakString(sArguments[iArg], sTime, sizeof(sTime))) != -1)
				{
					iArg += iNext_Arg;
				}
				else
				{
					iArg = 0;
					sArguments[0] = '\0';
				}
				
				int iT = StringToInt(sTime);
				
				BanClient(iTarget, iT, BANFLAG_AUTO, sArguments[iArg], sArguments[iArg], "sm_banplayer", iClient);
				
				ReplyToCommand(iClient, "[S.A.S.] Banned %N for %s", iTarget, sArguments[iArg]);
			} else {
				ReplyToCommand(iClient, "[S.A.S.] You do not have high enough admin privileges!");
				return Plugin_Handled;
			}
		} else {
			ReplyToCommand(iClient, "[S.A.S.] You do not have access to that command!");
			return Plugin_Handled;
		}
	} else {
		int iTarget = FindTarget(-1, sArg, false, false);
		
		if (iTarget == -1)
		{
			return Plugin_Handled;
		}
		
		if ((iNext_Arg = BreakString(sArguments[iArg], sTime, sizeof(sTime))) != -1)
		{
			iArg += iNext_Arg;
		}
		else
		{
			iArg = 0;
			sArguments[0] = '\0';
		}
		
		int iT = StringToInt(sTime);
		
		BanClient(iTarget, iT, BANFLAG_AUTO, sArguments[iArg], sArguments[iArg], "sm_banplayer", -1);
		
		ReplyToCommand(iClient, "[S.A.S.] Banned %N for %s", iTarget, sArguments[iArg]);
	}
	
	return Plugin_Handled;
}

public Action Command_CreateAdmin(int iClient, int iArgs)
{
	char sLevel[64], sTarget[128];
	
	if (iArgs < 2)
	{
		ReplyToCommand(iClient, "[S.A.S.] sm_createadmin <target> <admin level (basic, mod, root)>");
		return Plugin_Handled;
	}
	
	GetCmdArg(1, sTarget, sizeof(sTarget));
	GetCmdArg(2, sLevel, sizeof(sLevel));
	
	if (iClient)
	{
		if (SAS_CheckAdmin(iClient))
		{
			if (SAS_GetAdmin(iClient) >= 2)
			{
				int iTarget = FindTarget(iClient, sTarget, false, false);
				
				if (iTarget == -1)
				{
					return Plugin_Handled;
				}
				
				if (StrEqual(sLevel, "basic", false))
				{
					SAS_SetAdmin(iTarget, 1);
					
					ReplyToCommand(iClient, "[S.A.S.] Turned %N into a Basic Admin.", iTarget);
				} else if (StrEqual(sLevel, "mod", false))
				{
					SAS_SetAdmin(iTarget, 2);
					
					ReplyToCommand(iClient, "[S.A.S.] Turned %N into a Moderator.", iTarget);
				} else if (StrEqual(sLevel, "root", false))
				{
					SAS_SetAdmin(iTarget, 3);
					
					ReplyToCommand(iClient, "[S.A.S.] Turned %N into a Root Admin.", iTarget);
				} else if (StrEqual(sLevel, "", false))
				{
					ReplyToCommand(iClient, "[S.A.S.] sm_createadmin <target> <admin level (basic, mod, root)>");
					return Plugin_Handled;
				} else {
					ReplyToCommand(iClient, "[S.A.S.] sm_createadmin <target> <admin level (basic, mod, root)>");
					return Plugin_Handled;
				}
			} else {
				ReplyToCommand(iClient, "[S.A.S.] You do not have high enough admin privileges!");
				return Plugin_Handled;
			}
		} else {
			ReplyToCommand(iClient, "[S.A.S.] You do not have access to that command!");
			return Plugin_Handled;
		}
	} else {
		int iTarget = FindTarget(-1, sTarget, false, false);
		
		if (iTarget == -1)
		{
			return Plugin_Handled;
		}
		
		if (StrEqual(sLevel, "basic", false))
		{
			SAS_SetAdmin(iTarget, 1);
			
			ReplyToCommand(iClient, "[S.A.S.] Turned %N into a Basic Admin.", iTarget);
		} else if (StrEqual(sLevel, "mod", false))
		{
			SAS_SetAdmin(iTarget, 2);
			
			ReplyToCommand(iClient, "[S.A.S.] Turned %N into a Moderator.", iTarget);
		} else if (StrEqual(sLevel, "root", false))
		{
			SAS_SetAdmin(iTarget, 3);
			
			ReplyToCommand(iClient, "[S.A.S.] Turned %N into a Root Admin.", iTarget);
		} else if (StrEqual(sLevel, "", false))
		{
			ReplyToCommand(iClient, "[S.A.S.] sm_createadmin <target> <admin level (basic, mod, root)>");
			return Plugin_Handled;
		} else {
			ReplyToCommand(iClient, "[S.A.S.] sm_createadmin <target> <admin level (basic, mod, root)>");
			return Plugin_Handled;
		}
	}
	
	return Plugin_Handled;
}

public Action Command_SetNickname(int iClient, int iArgs)
{
	char sNickname[128], sTarget[128];
	
	if (iArgs < 2)
	{
		ReplyToCommand(iClient, "[S.A.S.] sm_setnickname <target> <nickname>");
		return Plugin_Handled;
	}
	
	GetCmdArg(1, sTarget, sizeof(sTarget));
	GetCmdArg(2, sNickname, sizeof(sNickname));
	
	if (iClient)
	{
		if (SAS_CheckAdmin(iClient))
		{
			if (SAS_GetAdmin(iClient) >= 2)
			{
				int iTarget = FindTarget(iClient, sTarget, false, false);
				
				if (iTarget == -1)
				{
					return Plugin_Handled;
				}
				
				SAS_SetNickname(iTarget, sNickname);
				
				ReplyToCommand(iClient, "[S.A.S.] Set %N's nickname to %s", iTarget, sNickname);
			} else {
				ReplyToCommand(iClient, "[S.A.S.] You do not have high enough admin privileges!");
				return Plugin_Handled;
			}
		} else {
			ReplyToCommand(iClient, "[S.A.S.] You do not have access to that command!");
			return Plugin_Handled;
		}
	} else {
		int iTarget = FindTarget(-1, sTarget, false, false);
		
		if (iTarget == -1)
		{
			return Plugin_Handled;
		}
		
		SAS_SetNickname(iTarget, sNickname);
		
		ReplyToCommand(iClient, "[S.A.S.] Set %N's nickname to %s", iTarget, sNickname);
	}
	
	return Plugin_Handled;
}

public Action Command_UnBanPlayer(int iClient, int iArgs)
{
	char sTarget[128];
	
	if (iArgs < 1)
	{
		ReplyToCommand(iClient, "[S.A.S.] sm_unbanplayer <steam id>");
		return Plugin_Handled;
	}
	
	GetCmdArg(1, sTarget, sizeof(sTarget));
	
	if (iClient)
	{
		if (SAS_CheckAdmin(iClient))
		{
			if (SAS_GetAdmin(iClient) >= 2)
			{
				RemoveBan(sTarget, BANFLAG_AUTHID, "sm_unbanplayer", iClient);
			} else {
				ReplyToCommand(iClient, "[S.A.S.] You do not have high enough admin privileges!");
				return Plugin_Handled;
			}
		} else {
			ReplyToCommand(iClient, "[S.A.S.] You do not have access to that command!");
			return Plugin_Handled;
		}
	} else {
		RemoveBan(sTarget, BANFLAG_AUTHID, "sm_unbanplayer", -1);
	}
	
	return Plugin_Handled;
}

//Plugin Natives:
public int Native_CheckAdmin(Handle hPlugin, int iNumParams)
{
	char sAuthID[64];
	
	int iClient = GetNativeCell(1);
	
	GetClientAuthId(iClient, AuthId_Steam2, sAuthID, sizeof(sAuthID), true);
	
	KeyValues kvAdmin = new KeyValues("Admins");
	
	kvAdmin.ImportFromFile(g_sAdminDatabase);
	
	if (kvAdmin.JumpToKey(sAuthID, false))
	{
		kvAdmin.Rewind();
		
		kvAdmin.Close();
		
		return true;
	} else {
		kvAdmin.Rewind();
		
		kvAdmin.Close();
		
		return false;
	}
}

public int Native_GetAdmin(Handle hPlugin, int iNumParams)
{
	char sAuthID[64];
	
	int iClient = GetNativeCell(1), iLevel;
	
	GetClientAuthId(iClient, AuthId_Steam2, sAuthID, sizeof(sAuthID), true);
	
	KeyValues kvAdmin = new KeyValues("Admins");
	
	kvAdmin.ImportFromFile(g_sAdminDatabase);
	
	if (kvAdmin.JumpToKey(sAuthID, false))
	{
		iLevel = kvAdmin.GetNum("level");
	} else {
		iLevel = 0;
	}
	
	kvAdmin.Rewind();
	
	kvAdmin.Close();
	
	return iLevel;
}

public int Native_GetNickname(Handle hPlugin, int iNumParams)
{
	char sAuthID[64], sNickname[128];
	
	int iClient = GetNativeCell(1), iLength = GetNativeCell(3);
	
	GetNativeString(2, sNickname, sizeof(sNickname));
	
	GetClientAuthId(iClient, AuthId_Steam2, sAuthID, sizeof(sAuthID), true);
	
	KeyValues kvNickname = new KeyValues("Nicknames");
	
	kvNickname.ImportFromFile(g_sNicknameDatabase);
	
	kvNickname.JumpToKey(sAuthID, false);
	
	kvNickname.GetString("nickname", sNickname, iLength, "null");
	
	kvNickname.Rewind();
	
	kvNickname.Close();
	
	SetNativeString(2, sNickname, iLength);
}

public int Native_SetAdmin(Handle hPlugin, int iNumParams)
{
	char sAuthID[64];
	
	int iClient = GetNativeCell(1), iLevel = GetNativeCell(2);
	
	GetClientAuthId(iClient, AuthId_Steam2, sAuthID, sizeof(sAuthID), true);
	
	KeyValues kvAdmin = new KeyValues("Admins");
	
	kvAdmin.ImportFromFile(g_sAdminDatabase);
	
	kvAdmin.JumpToKey(sAuthID, true);
	
	kvAdmin.SetNum("level", iLevel);
	
	kvAdmin.Rewind();
	
	kvAdmin.ExportToFile(g_sAdminDatabase);
	
	kvAdmin.Close();
}

public int Native_SetNickname(Handle hPlugin, int iNumParams)
{
	char sAuthID[64], sNickname[128];
	
	int iClient = GetNativeCell(1);
	
	GetNativeString(2, sNickname, sizeof(sNickname));
	
	GetClientAuthId(iClient, AuthId_Steam2, sAuthID, sizeof(sAuthID), true);
	
	KeyValues kvNickname = new KeyValues("Nicknames");
	
	kvNickname.ImportFromFile(g_sNicknameDatabase);
	
	kvNickname.JumpToKey(sAuthID, true);
	
	kvNickname.SetString("nickname", sNickname);
	
	kvNickname.Rewind();
	
	kvNickname.ExportToFile(g_sNicknameDatabase);
	
	kvNickname.Close();
}
