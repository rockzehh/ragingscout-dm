#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "RockZehh"
#define PLUGIN_VERSION "1.0.0a"

#include <sourcemod>
#include <morecolors>
#include <sdktools>
#include <tf2>
#include <tf2_stocks>
#include <smlib>

#pragma newdecls required

bool g_bJumpBoost[MAXPLAYERS + 1];

char g_sCreditsDatabase[PLATFORM_MAX_PATH];

Handle g_hCreditHud[MAXPLAYERS + 1];
Handle g_hStatHud[MAXPLAYERS + 1];

int g_iCredits[MAXPLAYERS + 1];
int g_iDeaths[MAXPLAYERS + 1];
int g_iKills[MAXPLAYERS + 1];

public Plugin myinfo = 
{
	name = "Raging Scout",
	author = PLUGIN_AUTHOR,
	description = "The backbone behind Raging Scout Deathmatch.",
	version = PLUGIN_VERSION,
	url = "https://bitbucket.org/rockzehh/ragingscout-dm"
};

public void OnPluginStart()
{
	HookEvent("player_death", Event_PlayerDeath);
	HookEvent("player_spawn", Event_PlayerSpawn);
	
	RegConsoleCmd("sm_credits", Command_Credits, "Brings up the credit menu.");
	RegConsoleCmd("sm_distort", Command_Distort, "Distorts your player model.");
	RegConsoleCmd("sm_boost", Command_JumpBoost, "Gives you a big jump boost.");
	
	BuildPath(Path_SM, g_sCreditsDatabase, PLATFORM_MAX_PATH, "data/ragingscout/credits.txt");
}

public void OnClientPutInServer(int iClient)
{
	g_bJumpBoost[iClient] = false;
	
	g_hCreditHud[iClient] = CreateTimer(0.1, Timer_CreditHud, iClient, TIMER_REPEAT);
	g_hStatHud[iClient] = CreateTimer(0.1, Timer_StatHud, iClient, TIMER_REPEAT);
	
	g_iDeaths[iClient] = 0;
	g_iKills[iClient] = 0;
	
	LoadClient(iClient);
}

public void OnClientDisconnect(int iClient)
{
	g_bJumpBoost[iClient] = false;
	
	CloseHandle(g_hCreditHud[iClient]);
	CloseHandle(g_hStatHud[iClient]);
	
	SaveClient(iClient);
	
	g_iCredits[iClient] = 0;
	g_iDeaths[iClient] = 0;
	g_iKills[iClient] = 0;
}

public Action OnPlayerRunCmd(int iClient, int &iButtons, int &iImpulse, float fVel[3], float fAngles[3], int &iWeapon, int &iSubtype, int &iCmdnum, int &iTickcount, int &iSeed, int iMouse[2])
{
	int iFlags = GetEntityFlags(iClient);
	float fVelocity[3];
	
	if(iButtons & IN_JUMP && g_bJumpBoost[iClient] && iFlags & FL_ONGROUND)
	{
		GetEntPropVector(iClient, Prop_Data, "m_vecVelocity", fVelocity);
		
		fVelocity[2] = 450.0;
		TeleportEntity(iClient, NULL_VECTOR, NULL_VECTOR, fVelocity);
	}
}

public void SaveClient(int iClient)
{
	char sAuthID[64];

	GetClientAuthId(iClient, AuthId_Steam2, sAuthID, sizeof(sAuthID));

	KeyValues kvVault = new KeyValues("Credits");
	
	kvVault.ImportFromFile(g_sCreditsDatabase);

	SaveInteger(kvVault, sAuthID, "Credits", g_iCredits[iClient]);

	kvVault.ExportToFile(g_sCreditsDatabase);

	kvVault.Close();
}

public void SaveInteger(KeyValues kvVault, char[] sKey, char[] sSaveKey, int iVariable)
{
	if(iVariable == -1)
	{
		kvVault.JumpToKey(sKey, true);
		
		kvVault.DeleteKey(sSaveKey);

		kvVault.Rewind();

	}else{
		kvVault.JumpToKey(sKey, true);

		kvVault.SetNum(sSaveKey, iVariable);
	
		kvVault.Rewind();
	}
}

public int LoadInteger(KeyValues kvVault, char[] sKey, char[] sSaveKey, int iDefaultValue)
{
	kvVault.JumpToKey(sKey, false);

	int iVariable = kvVault.GetNum(sSaveKey, iDefaultValue);

	kvVault.Rewind();

	return iVariable;
}

public void LoadClient(int iClient)
{
	char sAuthID[64];

	GetClientAuthId(iClient, AuthId_Steam2, sAuthID, sizeof(sAuthID));

	KeyValues kvVault = new KeyValues("Credits");

	kvVault.ImportFromFile(g_sCreditsDatabase);

	kvVault.JumpToKey(sAuthID, false);

	int iCredits = LoadInteger(kvVault, sAuthID, "Credits", 1500);

	kvVault.Rewind();

	g_iCredits[iClient] = iCredits;

	kvVault.Close();
}

public Action Event_PlayerDeath(Handle hEvent, char[] sName, bool bDontBroadcast)
{
	int iClient = GetClientOfUserId(GetEventInt(hEvent, "userid"));
	int iAttacker = GetClientOfUserId(GetEventInt(hEvent, "attacker"));
	
	int iRandom = GetRandomInt(7, 16);
	
	if(iAttacker != iClient)
	{
		if(iAttacker == -1)
		{
			g_iDeaths[iClient]++;
		}else{
			g_iKills[iAttacker]--;
			g_iDeaths[iClient]++;
		
			g_iCredits[iAttacker] += iRandom;
		
			CPrintToChatAll("{green}%N{default} got {green}%i{default} credits for killing {green}%N{default}!", iAttacker, iRandom, iClient);
		
			SaveClient(iClient);	
		}
	}else{
		g_iKills[iClient]--;
		g_iDeaths[iClient]++;
		
		g_iCredits[iClient] -= iRandom;
		
		CPrintToChatAll("{green}%N{default} has lost {green}%i{default} credits for killing themselves.", iClient, iRandom);
	}
}

public Action Event_PlayerSpawn(Handle hEvent, char[] sName, bool bDontBroadcast)
{
	int iClient = GetClientOfUserId(GetEventInt(hEvent, "userid"));
	
	TF2_SetPlayerClass(iClient, TFClass_Scout);
}

public Action Command_Distort(int iClient, int iArgs)
{
	if(g_iCredits[iClient] <= 500)
	{
		CPrintToChat(iClient, "{red}Raging Scout{default}: You do not have enough credits.");
	}else{
		g_iCredits[iClient] -= 500;

		CPrintToChat(iClient, "{red}Raging Scout{default}: You have bought {green}distort{default} for {green}500{default} credits for {green}60{default} seconds.");

		SetEntityRenderFx(iClient, RENDERFX_DISTORT);
		
		CreateTimer(60.0, Timer_Visible, iClient);
		
		SaveClient(iClient);
	}
	
	return Plugin_Handled;
}

public Action Command_JumpBoost(int iClient, int iArgs)
{
	if(g_iCredits[iClient] <= 175)
	{
		CPrintToChat(iClient, "{red}Raging Scout{default}: You do not have enough credits.");
	}else{
		g_iCredits[iClient] -= 175;

		CPrintToChat(iClient, "{red}Raging Scout{default}: You have bought {green}jump-boost{default} for {green}175{default} credits for {green}120{default} seconds.");

		g_bJumpBoost[iClient] = true;
		
		CreateTimer(120.0, Timer_JumpBoost, iClient);
		
		SaveClient(iClient);
	}
	
	return Plugin_Handled;
}

public int Menu_Credits(Menu hMenu, MenuAction iAction, int iParam1, int iParam2)
{
	switch(iAction)
	{
		case MenuAction_Start:
		{
			PrintToServer("Displaying menu");
		}
 
		case MenuAction_Display:
		{}
 
		case MenuAction_Select:
		{
			char sInfo[64];
			
			hMenu.GetItem(iParam2, sInfo, sizeof(sInfo));
			
			if(StrEqual(sInfo, "opt_distort"))
			{
				FakeClientCommand(iParam1, "sm_distort");
			}else if(StrEqual(sInfo, "opt_boost"))
			{
				FakeClientCommand(iParam1, "sm_boost");
			}
		}
 
		case MenuAction_Cancel:
		{}
 
		case MenuAction_End:
		{
			delete hMenu;
		}
 
		case MenuAction_DrawItem:
		{}
 
		case MenuAction_DisplayItem:
		{}
	}
 
	return 0;
}
 
public Action Command_Credits(int iClient, int iArgs)
{
	Menu hMenu = new Menu(Menu_Credits, MENU_ACTIONS_ALL);
	
	hMenu.SetTitle("Credit Menu (%i credits)", g_iCredits[iClient]);
	
	hMenu.AddItem("opt_distort", "Distort | 500 Credits");
	hMenu.AddItem("opt_boost", "Jump-Boost | 175 Credits");
	
	hMenu.ExitButton = true;
	
	hMenu.Display(iClient, MENU_TIME_FOREVER);
 
	return Plugin_Handled;
}

public Action Timer_CreditHud(Handle hTimer, any iClient)
{
	char sCreditHud[128];
	
	if(IsClientInGame(iClient))
	{
		Format(sCreditHud, sizeof(sCreditHud), "Name: %N\nCredits: %i", iClient, g_iCredits[iClient]);
		
		SetHudTextParams(-0.010, 0.010, 0.5, 255, 128, 0, 128, 0, 0.1, 0.1, 0.1);
		ShowHudText(iClient, -1, sCreditHud);	
	}
}

public Action Timer_JumpBoost(Handle hTimer, any iClient)
{
	if(IsClientConnected(iClient))
	{
		g_bJumpBoost[iClient] = false;
		
		CPrintToChat(iClient, "{red}Raging Scout{default}: Your {green}jump-boost{default} has worn off.");
	}
}

public Action Timer_StatHud(Handle hTimer, any iClient)
{
	char sStatsHud[128];
	
	float fKTD;
	
	int iTimeleft;
	
	GetMapTimeLeft(iTimeleft);
	
	if(IsClientInGame(iClient))
	{
		fKTD = ( (g_iDeaths[iClient] == 0) ? 0.0 : FloatDiv( float(g_iKills[iClient]), float(g_iDeaths[iClient]) ) );
		
		Format(sStatsHud, sizeof(sStatsHud), "Stats:\n%i Kills\n%i Deaths\n%.1f KTD Ratio\n\nTimeleft: %d:%02d", g_iKills[iClient], g_iDeaths[iClient], fKTD, (iTimeleft / 60), (iTimeleft % 60));
		
		SetHudTextParams(0.010, 0.010, 0.5, 255, 128, 0, 128, 0, 0.1, 0.1, 0.1);
		ShowHudText(iClient, -1, sStatsHud);	
	}
}

public Action Timer_Visible(Handle hTimer, any iClient)
{
	if(IsClientConnected(iClient))
	{
		SetEntityRenderFx(iClient, RENDERFX_NONE);
		
		CPrintToChat(iClient, "{red}Raging Scout{default}: Your {green}distort{default} has worn off.");
	}
}
