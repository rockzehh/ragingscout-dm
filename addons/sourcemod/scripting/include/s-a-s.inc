/**
 * Simple Administration System
 * Developed by Fusion Developments - 2017
 */

#if defined _sas_included
#endinput
#endif
#define _sas_included

/**
 * Checks if a client is an administrator.
 *
 * @param iClient	Client Index
 * @return	If True Then Client Is An Admin, If False Then Client Isn't An Admin
 *
 */
native bool SAS_CheckAdmin(int iClient);

/**
 * Gets the clients admin level. Returns 0 if the client isn't an admin.
 *
 * @param iClient	Client Index
 * @return Returns The Client Admin Level. Returns 0 If The Client Isn't An Admin
 *
 */
native int SAS_GetAdmin(int iClient);

/**
 * Gets the clients nickname. Returns "null" if the client doesn't have a nickname.
 *
 * @param iClient	Client Index
 * @param sBuffer	Buffer To Store The Nickname.
 * @param iLength	Length Of Buffer
 *
 */
native char SAS_GetNickname(int iClient, char[] sBuffer, int iLength);

/**
 * Sets the clients admin level.
 *
 * @param iClient	Client Index
 * @param iLevel	Admin Level
 *
 */
native void SAS_SetAdmin(int iClient, int iLevel);

/**
 * Sets the client nickname.
 *
 * @param iClient	Client Index
 * @param sNickname	Nickname Buffer
 *
 */
native void SAS_SetNickname(int iClient, const char[] sNickname); 